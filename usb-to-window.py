#!/usr/bin/env python3

import traceback
import sys
import argparse

import gi
gi.require_version('Gst', '1.0')
from gi.repository import Gst, GObject, GLib

def print_pipeline_info( pl ):
    e_names = ["input", "output_vid", "output_screen", "inputstream"]
    for n in e_names:
        el = pl.get_by_name( n )
        for e_dir in ["sink", "src"]:
            pad = el.get_static_pad(e_dir)
            if pad:
                caps = pad.get_current_caps()
                if not caps:
                    caps = pad.get_allowed_caps()

                if caps:
                    print( n, e_dir, ":", caps.get_size())
                    try:
                        for c in caps:
                            print( "-", c)
                    except TypeError:
                        print( "-", caps.to_string())
                else:
                    print( n, e_dir, ": none (no caps)" )
            else:
                print( n, e_dir, ": none (no pads)" )

def on_message(bus, message, loop):
    mtype = message.type
    """
        Gstreamer Message Types and how to parse
        https://lazka.github.io/pgi-docs/Gst-1.0/flags.html#Gst.MessageType
    """
    if mtype == Gst.MessageType.EOS:
        print("End of stream")
        return False

    elif mtype == Gst.MessageType.ERROR:
        err, debug = message.parse_error()
        print(err, debug)
    elif mtype == Gst.MessageType.WARNING:
        err, debug = message.parse_warning()
        print(err, debug)

#    else:
#        print("Got non-error and non-warning message type: {}".format(message.type))

    return True


def main( dev_in, dev_out, show_caps ):
    # Initializes Gstreamer, it's variables, paths
    Gst.init(sys.argv)

    command = "v4l2src device={} name=input ! video/x-raw,framerate=(fraction)30/1 ! tee name=inputstream \
          inputstream. ! queue ! vaapisink fullscreen=true name=output_screen  \
          inputstream. ! queue ! v4l2sink device={} name=output_vid".format( dev_in, dev_out )
    # Gst.Pipeline https://lazka.github.io/pgi-docs/Gst-1.0/classes/Pipeline.html
    # https://lazka.github.io/pgi-docs/Gst-1.0/functions.html#Gst.parse_launch
    try:
        pipeline = Gst.parse_launch(command)
    except GLib.Error as ex:
        print("Error parsing pipeline string")
        print("String was: >{}<".format(command))
        return

    if show_caps:
        print("Pipeline parsed. Listing allowed caps.")
        print_pipeline_info( pipeline )

    # https://lazka.github.io/pgi-docs/Gst-1.0/classes/Bus.html
    bus = pipeline.get_bus()
    # allow bus to emit messages to main thread
    bus.add_signal_watch()
    # Add handler to specific signal
    # https://lazka.github.io/pgi-docs/GObject-2.0/classes/Object.html#GObject.Object.connect
    bus.connect("message", on_message, None)
    # Start pipeline
    pipeline.set_state(Gst.State.PLAYING)

    # Init GObject loop to handle Gstreamer Bus Events
    loop = GLib.MainLoop()
    try:
        if show_caps:
            print("Pipeline started. Listing current caps.")
            print_pipeline_info( pipeline )

        print("Starting main loop")
        loop.run()
    except KeyboardInterrupt:
        print("CTRL+C caught. Exiting.")
        pipeline.set_state(Gst.State.NULL)
        return
    except:
        print("Some exception occurred")
        traceback.print_exc()
    # Stop Pipeline

    print("Stopping pipeline")
    pipeline.set_state(Gst.State.NULL)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--show-caps', dest="show_caps", help="show gstreamer caps",
            action='store_const', const=True )
    parser.add_argument('--in',  dest="videodev_in", help="input device", default="/dev/video0")
    parser.add_argument('--out', dest="videodev_out", help="output device", default="/dev/video10")
    args = parser.parse_args()

    main( args.videodev_in, args.videodev_out, args.show_caps )
