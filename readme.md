videosys
=============

Installing base packages
-------------------------

As root, install virtual videodevices and gsteamer plugins

```
apt-get install virtualenv
apt-get install v4l2loopback-utils v4l2loopback-dkms
apt-get install gstreamer1.0-vaapi
```

As root, create the virtual devices
```
modprobe v4l2loopback video_nr=10 card_label="input stream" -v
```


Virtual environments
--------------------

We have a virtualenvironment for the python stuff

```
virtualenv -p python3 venv
source venv/bin/activate
pip3 install -r requirements
```

Usage
------------

### Pipe v4l2 source to secondary screen

`python3 usb-to-window.py`


Debugging
-----------

E.g.
```
GST_DEBUG=videotestsrc:5 videotestsrc num-buffers=50 ! xvimagesink
```
will give a lot of info about `videotestsrc`.

Usefull with `v4l2src` to query possible caps.
